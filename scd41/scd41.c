/*
 * File:   scd41.c
 * Author: Miroslav Soukup
 * Description: Source file of scd41 CO2 sensor driver.
 * 
 */



// *****************************************************************************
// Section: Included Files
// *****************************************************************************

#include <stdint.h> // data types
#include <stdlib.h>

#include "scd41.h"


// *****************************************************************************
// Section: Functions
// *****************************************************************************

static uint8_t _scd41_sensirion_common_generate_crc(const uint8_t* data, uint16_t count)
{
    uint8_t crc = 0xFF;
    
    /* calculates 8-Bit checksum with given polynomial */
    for (uint16_t current_byte = 0; current_byte < count; ++current_byte) {
        
        crc ^= (data[current_byte]);
    
        for (uint8_t crc_bit = 8; crc_bit > 0; --crc_bit) {
            if (crc & 0x80)
                crc = (crc << 1) ^ 0x31; // CRC8 polynomial 0x31
             else
                crc = (crc << 1);
        }
    }
    
    return crc;
}

static uint8_t _scd41_i2c_write_reg(scd41_t *me, uint16_t reg, uint16_t data)
{
    if(!me) return 0;
    
    uint8_t _data[5];
    _data[0] = reg >> 8;
    _data[1] = reg;
    _data[2] = data >> 8;
    _data[3] = data;
    _data[4] = _scd41_sensirion_common_generate_crc(&_data[2], 2);
    if(!me->i2c_write(me->client_address, _data, 5)) return 0;
    
    return 1;
}

static uint8_t _scd41_i2c_write_cmd(scd41_t *me, uint16_t cmd)
{
    if(!me) return 0;
    
    uint8_t _data[2];
    _data[0] = cmd >> 8;
    _data[1] = cmd;
    if(!me->i2c_write(me->client_address, _data, 2)) return 0;
    
    return 1;
}

static uint8_t _scd41_i2c_read_reg(scd41_t *me, uint16_t reg, uint16_t *data, uint8_t cnt)
{
    if(!me) return 0;
    
    uint8_t _data[9];
    _data[0] = reg >> 8;
    _data[1] = reg;
    if(!me->i2c_write(me->client_address, _data, 2)) return 0;
    if(!me->i2c_read(me->client_address, _data, cnt * 3)) return 0;
    for(uint8_t i = 0; i < cnt; ++i){
        if(_scd41_sensirion_common_generate_crc(&_data[i * 3], 2) != _data[(i * 3) + 2]) return 0;
        data[i] = _data[i * 3] << 8 | _data[(i * 3) + 1];
    }
    
    return 1;
}

uint8_t scd41_init (scd41_t *me, uint16_t client_address, scd41_config_t *config)
{
    if(!me || !config) return 0;
    
    me->config = *config;
    me->client_address = client_address;
    
    me->status.data_ready = SCD41_STATUS_DATA_NOT_READY;
    
    me->delay_ms(1000);
    
    if(!_scd41_i2c_write_cmd(me, SCD41_CMD_MEASUREMENT_PERIODIC_STOP)) return 0; // stop periodic measuremet
    me->delay_ms(500);
    
    uint16_t serial_number_data[3];
    if(!_scd41_i2c_read_reg(me, SCD41_GET_SERIAL_NUMBER, serial_number_data, 3)) return 0; // get serial number
    me->data.serial_number = (((uint64_t) serial_number_data[0]) << 32) | (((uint64_t) serial_number_data[1]) << 16) | ((uint64_t) serial_number_data[2]);
    
    if(!_scd41_i2c_write_reg(me, SCD41_SET_TEMPERATURE_OFFSET, (uint16_t) (65536.0 * (me->config.temp_offset / 175.0)))) return 0; // set temperature offset
    
    if(!_scd41_i2c_write_reg(me, SCD41_SET_ALTITUDE, (uint16_t) me->config.altitude)) return 0; // set sensor altitude
    
    if(!_scd41_i2c_write_reg(me, SCD41_SET_AMBIENT_PRESSURE, (uint16_t) (me->config.ambient_press / 100))) return 0; // set ambient pressure
    
    if(!_scd41_i2c_write_reg(me, SCD41_SET_AUTOMATIC_SELF_CALIBRATION, (uint16_t) me->config.auto_self_calib)) return 0; // set automatic self-calibration
    
    if(me->config.pers_settings) if(!_scd41_i2c_write_cmd(me, SCD41_CMD_PERSIST_SETTINGS)) return 0; // persist settings (only is necessary)
    
    if(me->config.meas_mode == SCD41_CONFIG_MEAS_MODE_PERIODIC) { if(!_scd41_i2c_write_cmd(me, SCD41_CMD_MEASUREMENT_PERIODIC_START)) return 0; } // start periodic measuremet
    else if(me->config.meas_mode == SCD41_CONFIG_MEAS_MODE_PERIODIC_LOW_POWER) { if(!_scd41_i2c_write_cmd(me, SCD41_CMD_MEASUREMENT_PERIODIC_LOW_POWER_START)) return 0; }
    
    return 1;
}

uint8_t scd41_i2c_write_register (scd41_t *me, scd41_i2c_rw_funcptr_t i2c_write_funcptr)
{
    if(!me || !i2c_write_funcptr) return 0;
    me->i2c_write = i2c_write_funcptr;
    return 1;
}

uint8_t scd41_i2c_read_register (scd41_t *me, scd41_i2c_rw_funcptr_t i2c_read_funcptr)
{
    if(!me || !i2c_read_funcptr) return 0;
    me->i2c_read = i2c_read_funcptr;
    return 1;
}

uint8_t scd41_delay_ms_register (scd41_t *me, scd41_delay_ms_funcptr_t delay_ms_funcptr)
{
    if(!me || !delay_ms_funcptr) return 0;
    me->delay_ms = delay_ms_funcptr;
    return 1;
}

uint8_t scd41_get_data (scd41_t *me)
{
    if(!me) return 0;
    
    uint16_t _data[3];
    if(!_scd41_i2c_read_reg(me, SCD41_GET_MEASUREMENT, _data, 3)) return 0;
    
    me->data.co2  = (uint16_t) _data[0];
    me->data.temp = (int8_t) (-45.0 + 175.0 * (_data[1] / 65536.0));
    me->data.hum  = (uint8_t) (100.0 * (_data[2] / 65536.0));
    
    return 1;
}

uint8_t scd41_get_serial_number (scd41_t *me)
{
    if(!me) return 0;
    
    uint16_t sn[3];
    if(!_scd41_i2c_read_reg(me, SCD41_GET_SERIAL_NUMBER, sn, 3)) return 0;
    me->data.serial_number = (((uint64_t) sn[0]) << 32) | (((uint64_t) sn[1]) << 16) | ((uint64_t) sn[2]);;
    
    return 1;
}

uint8_t scd41_i2c_check (scd41_t *me)
{
    if(!me) return 0;
    uint8_t _byte;
    return me->i2c_read(me->client_address, &_byte, 1);
}

uint8_t scd41_get_device_status (scd41_t *me)
{
    if(!me) return 0;
    uint16_t _word;
    if(!_scd41_i2c_read_reg(me, SCD41_GET_DATA_READY_STATUS, &_word, 1)) return 0;
    me->status.data_ready = (_word & 0b0000011111111111) ? 1 : 0;
    return 1;
}

uint8_t scd41_perform_persist_settings (scd41_t *me)
{
    if(!me) return 0;
    return _scd41_i2c_write_cmd(me, SCD41_CMD_PERSIST_SETTINGS);
}

uint8_t scd41_perform_factory_reset (scd41_t *me)
{
    if(!me) return 0;
    return _scd41_i2c_write_cmd(me, SCD41_CMD_PERFORM_FACTORY_RESET);
}

uint8_t scd41_perform_measure_single_shot (scd41_t *me)
{
    if(!me) return 0;
    if(me->config.meas_mode == SCD41_CONFIG_MEAS_MODE_SINGLE_SHOT) if(!_scd41_i2c_write_cmd(me, SCD41_CMD_MEASUREMENT_SINGLE_START)) return 0;
    return 1;
}

void scd41_get_default_config(scd41_config_t *config)
{
    if(!config) return;
    config->temp_offset     = 4.5;   // +4.5 degrees temperature calibration
    config->altitude        = 310;   // Czech Republic - Pilsen
    config->ambient_press   = 99500; // 995 hPa
    config->auto_self_calib = SCD41_CONFIG_AUTO_SELF_CALIB_ON;
    config->meas_mode       = SCD41_CONFIG_MEAS_MODE_PERIODIC; // 5s
    config->pers_settings   = SCD41_CONFIG_PERS_SETTINGS_DO_NOT_SAVE;
}
