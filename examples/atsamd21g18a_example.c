#include <stddef.h>
#include <stdbool.h>
#include <stdlib.h>
#include "definitions.h"

#include "scd41/scd41.h"


uint8_t i2c_read(uint16_t client_address, uint8_t *data, uint8_t size){
    SERCOM1_I2C_Read(client_address, data, size);
    while(SERCOM1_I2C_IsBusy());
    return !(SERCOM1_I2C_ErrorGet());
}

uint8_t i2c_write(uint16_t client_address, uint8_t *data, uint8_t size){
    SERCOM1_I2C_Write(client_address, data, size);
    while(SERCOM1_I2C_IsBusy());
    return !(SERCOM1_I2C_ErrorGet());
}

void delay_ms(uint32_t ms){
    SYSTICK_TimerStart();
    SYSTICK_DelayMs(ms);
    SYSTICK_TimerStop();
}


int main(void)
{
    SYS_Initialize (NULL);
    
    
    scd41_t scd41 = {};
    
    scd41_i2c_read_register(&scd41, i2c_read);
    scd41_i2c_write_register(&scd41, i2c_write);
    scd41_delay_ms_register(&scd41, delay_ms);
    
    scd41_config_t config = {};
    config.temp_offset     = 4.5;   // +4.5 degrees temperature calibration
    config.altitude        = 472;   // 471 m above the sea level
    config.ambient_press   = 99500; // 995 hPa
    config.auto_self_calib = SCD41_CONFIG_AUTO_SELF_CALIB_ON;
    config.meas_mode       = SCD41_CONFIG_MEAS_MODE_PERIODIC_LOW_POWER;
    config.pers_settings   = SCD41_CONFIG_PERS_SETTINGS_DO_NOT_SAVE;
    
    if(!scd41_init(&scd41, SCD41_I2C_CLIENT_ADDRESS, &config)){
        printf("SCD41 INIT ERROR\r\n");
    }
    else{
        printf("SCD41 serial number is %llu\r\n", scd41.data.serial_number);
    }
    
    while(true)
    {
        SYS_Tasks();
        
        if(!scd41_get_device_status(&scd41)) printf("I2C ERROR\r\n");
        
        if(scd41.status.data_ready == SCD41_STATUS_DATA_READY){
            if(scd41_get_data(&scd41)){
                printf("CO2 = %u ppm, TEMP = %d C, RH = %u %%\r\n", scd41.data.co2, scd41.data.temp, scd41.data.hum);
            }
        }
        
    }
    
    return ( EXIT_FAILURE );
}
